#!/bin/sh

[ $# -eq 0 ] && printf "Usage: encrypt.sh <inputfile|inputfile outputfile|-n inputfile>\n\t-n : encrypt filename\n\n" && exit 1
[ $# -gt 2 ] && printf "Usage: encrypt.sh <inputfile|inputfile outputfile|-n inputfile>\n\t-n : encrypt filename\n\n" && exit 1

if [ "$CRIPTAPWD" != "" ]; then
    PASSWORD=$CRIPTAPWD
else 
    read -s -p "Password: " PASS1
    echo
    read -s -p "Confirm password: " PASS2
    echo

    [ "$PASS1" != "$PASS2" ] && echo "passwords do not match. giving up." && exit 1
    PASSWORD=$PASS1
fi

if [ $# -eq 1 ]; then
    INFN=$1
    OUTFN=$1.enc
else 
    if [ "$1" = "-n" ]; then
        INFN=$2
        OUTFN=`printf "$2" | openssl enc -aes-128-cbc -pass pass:$PASSWORD | hexdump -ve '1/1 "%.2x"' 2> /dev/null`.enc
        [ $? -ne 0 ] && echo "Error encrypting filename. Giving up." && exit 1
    else 
        INFN=$1
        OUTFN=$2
    fi
fi

[ ! -f "$INFN" ] && echo "Specified file does not exist or is not a file. Giving up." && exit 1
[ ! -r "$INFN" ] && echo "Specified file is not readable. Giving up." && exit 1

[ -e "$OUTFN" ] && echo "Specified destination file already exists. Giving up." && exit 1

openssl aes-256-cbc -pass pass:$PASSWORD -in $INFN -out $OUTFN 2> /dev/null
[ $? -ne 0 ] && echo "Error encrypting file. Giving up." && exit 1
