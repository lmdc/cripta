#!/bin/sh


[ $# -ge 3 ] && printf "Usage: decrypt.sh [-n] inputfile\n\t-n : decrypt filename\n\n" && exit 1
[ $# -eq 2 ] && [ "$1" != "-n" ] && printf "Usage: decrypt.sh [-n] inputfile\n\t-n : decrypt filename\n\n" && exit 1

if [ "$1" = "-n" ]; then
  [ ! -f "$2" ] && echo "Specified file does not exist or is not a file. Giving up." && exit 1
  [ ! -r "$2" ] && echo "Specified file is not readable. Giving up." && exit 1
  INFN=$2
  EFN=`basename $INFN .enc`
  OUTFN=`echo $EFN | xxd -r -p  | openssl enc -d -aes-128-cbc -pass pass:$PASSWORD 2> /dev/null`
  [ $? -ne 0 ] && echo "error decrypting filename. Giving up." && exit 1
else
  [ $# -ne 1 ] && printf "Usage: decrypt.sh [-n] inputfile\n\t-n : decrypt filename\n\n" && exit 1
  [ ! -f "$1" ] && echo "Specified file does not exist or is not a file. Giving up." && exit 1
  [ ! -r "$1" ] && echo "Specified file is not readable. Giving up." && exit 1
  INFN=$1
  OUTFN=`basename $1 .enc` 
fi


[ -e "$OUTFN" ] && echo "file already exists ($OUTFN). Giving up" && exit 1

if [ "$CRIPTAPWD" != "" ]; then
    PASSWORD=$CRIPTAPWD
else
    read -s -p "Password: " PASSWORD
    echo
fi

openssl aes-256-cbc -pass pass:$PASSWORD -d -in $INFN -out $OUTFN 2> /dev/null
[ $? -ne 0 ] && echo "error decrypting file. Giving up." && exit 1

echo "decrypted to $OUTFN"
